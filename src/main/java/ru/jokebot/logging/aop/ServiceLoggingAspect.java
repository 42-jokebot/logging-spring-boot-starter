package ru.jokebot.logging.aop;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.util.StopWatch;
import ru.jokebot.logging.annotation.Logged;
import ru.jokebot.logging.config.LoggingProperties;

/**
 * The {@code ServiceLoggingAspect} is an AspectJ aspect responsible for logging method execution of methods
 * in classes and methods marked {@link Logged @Logged}.
 * <p>
 * It log information about the called method, the return value, and (optional) the execution time of the request.
 * </p>
 */
@Slf4j
@Aspect
@RequiredArgsConstructor
public class ServiceLoggingAspect {

    private final LoggingProperties properties;

    @Pointcut("@within(ru.jokebot.logging.annotation.Logged) || @annotation(ru.jokebot.logging.annotation.Logged)")
    public void isLogged() {
    }

    @Pointcut("@within(org.springframework.stereotype.Service)")
    public void callService() {
    }

    @Around("isLogged() && callService()" +
            "&& target(service)")
    public Object addLoggingAround(ProceedingJoinPoint proceedingJoinPoint, Object service) throws Throwable {
        Object result;
        String methodName = proceedingJoinPoint.getSignature().getName();
        try {
            String logMessage;
            if (properties.isTimeLogging()) {
                StopWatch stopWatch = new StopWatch();

                stopWatch.start();
                result = proceedingJoinPoint.proceed();
                stopWatch.stop();

                logMessage = "Invoked %s in class %s, result %s | exec in %s ms"
                        .formatted(methodName, service, result, stopWatch.getTotalTimeMillis());
            } else {
                result = proceedingJoinPoint.proceed();
                logMessage = "Invoked %s in class %s, result %s".formatted(methodName, service, result);
            }
            log.info(logMessage);
        } catch (Throwable ex) {
            log.error("Invoked {} in class {}, exception {}: {}", methodName, service, ex.getClass(), ex.getMessage());
            throw ex;
        }

        return result;
    }
}
